try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

def readme():
    with open('README.rst') as f:
        return f.read()
	
setup(name='availsightdist',
      version='0.1',
      description='Compute Available Sight Distance from MLS LIDAR Data',
      long_description=readme(),
      classifiers=[
          'Development Status :: 1 - Alpha',
          'License :: OSI Approved :: MIT License',
          'Programming Language :: Python :: 3.4',
          'Topic :: Transportation Engineering :: LIDAR',
      ],
      keywords='transportation engineering LIDAR available sight distance',
      url='https://github.com/jchkoch/availsightdist',
      author='James Koch',
      author_email='jckoch@ualberta.ca',
      license='MIT',
      packages=['availsightdist'],
      install_requires=[],
      test_suite='nose.collector',
      tests_require=['nose', 'nose-cover3'],
      entry_points={},
      include_package_data=True,
      zip_safe=False)
