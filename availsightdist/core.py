#! /usr/bin/python3

import os
import glob
#import laspy as lp

class interface:
    def user_inputs():
        # Ask user for the number of the Highway to be analyzed
        hwy = input('Please enter the Highway number (i.e. ##): ')
        # Ask for directory where the LIDAR file(s) are located
        path = input('Please enter the directory where the LIDAR LAS files are located: ') 
        # Ask for elevation adjustment for vehicle path (observer and target heights)
        adj = input('Please enter the observer vehicle elevation adjustment: ')
        adj += input('Please enter the target vehicle elevation adjustment: ')
        return(hwy,path,adj)
    
    def get_lasfiles(path):
        print(path)
        os.chdir(path)
        files = glob.glob('*.las')
        return(files)
    
    hwy,path,adj = user_inputs()
    files = get_lasfiles(path)
    print(files)
_
# class vehpath(self,files,adj):
#     def loadlidar(f):
#         fname,ext = os.path.splitext(f)
#         if ext == '.las':
#             lasfile =    
